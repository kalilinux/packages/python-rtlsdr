python-rtlsdr (0.3.0-0kali1) kali-dev; urgency=medium

  * New upstream version 0.3.0
  * Update build-deps and deps
  * Remove useless patches

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 17 May 2024 16:03:00 +0200

python-rtlsdr (0.2.91-0kali3) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * Update watch file format version to 4.
  * Update standards version to 4.6.1, no changes needed.
  * Set upstream metadata fields: Repository-Browse.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.2, no changes needed.

  [ Ben Wilson ]
  * Update email address

  [ Sophie Brun ]
  * Update build-deps and deps: librtlsdr2 replaces librtlsdr0
  * Update uploaders

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 21 Dec 2023 15:31:05 +0100

python-rtlsdr (0.2.91-0kali2) kali-dev; urgency=medium

  * Remove Python 2 package
  * Bump Standards-Version to 4.5.0

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 06 Apr 2020 15:34:55 +0200

python-rtlsdr (0.2.91-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Vcs-* fields for the move to gitlab.com
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 0.2.91
  * Add Python 3 module
  * Refresh patch
  * Use debhelper-compat 12
  * Bump Standards-Version to 4.4.1

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 24 Jan 2020 15:22:15 +0100

python-rtlsdr (0.2.9-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 07 Nov 2018 15:23:17 +0100

python-rtlsdr (0.2.4-0kali1) kali-dev; urgency=medium

  * Import new upstream release (closes: 0003779)

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 19 Dec 2016 13:50:04 +0100

python-rtlsdr (0.1.1+git20130829-1kali1) kali-dev; urgency=medium

  * Update depends with new names of packages: rtl-sdr instead of
    librtlsdr-bin and librtlsdr0 instead of librtlsdr
  * Use debhelper 9 and fix vcs-git

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 08 Aug 2014 15:49:58 +0200

python-rtlsdr (0.1.1+git20130829-1kali0) kali; urgency=low

  * Initial release (Closes: 0000559)

 -- Devon Kearns <dookie@kali.org>  Thu, 29 Aug 2013 12:07:10 -0600
